/* AngularJS Function for Restore My Drive */
(function() {
    var app = angular.module('myApp', []);
    
    var search_params = {}

    app.controller('TabController', function(){
        this.tab = 1;

        this.setTab = function(newValue){
        this.tab = newValue;
        };

        this.isSet = function(tabName){
        return this.tab === tabName;
        };
    });
    
    app.controller('restoreFormController', ['$scope', function($scope){
        $scope.restore_params = {};
        
        $scope.addParams = function(params){
            $scope.restore_params = angular.copy(params)
            q_string = 'title contains "' + $scope.restore_params.keyword + '"'
            //listFiles(q_string)
        };
        
        function listFiles(q_string) {
                var request = gapi.client.drive.files.list({
                    'maxResults': 10,
                    'q': q_string,
                    'fields': 'items(id,lastModifyingUserName,mimeType,modifiedDate,title)'
                  });

                  request.execute(function(resp) {
                    appendPre('Files:');
                    var files = resp.items;
                    if (files && files.length > 0) {
                      for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        appendPre(file.title + '||' + file.id + '||' + file.lastModifyingUserName)
                        //appendPre(file.title + '|| (' + file.id + ')');
                      }
                    } else {
                      appendPre('No files found.');
                    }
                  });
        };
    }]);
    
})();
