# -*- coding: utf-8 -*-
"""
Description:

Author: David Kennedy
Copyright:
Credits
License
Version
Maintainer: David Kennedy
Email: david.kennedy48@gmail.com
Status: Development
"""
from flask import Flask, jsonify
from flask import render_template
from flask import request, abort, stream_with_context, Response
from flask_wtf.form import Form
from wtforms import StringField, DateField, SelectField
from flask_triangle import Triangle
import json

from authorization import authorize, revoke_access
from find_files import FindFiles
from search_for_files import SearchForFiles

app = Flask(__name__)
Triangle(app)

class RestoreForm(Form):
    keyword = StringField('Keyword:')


@app.route('/_add_numbers')
def add_numbers():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    r = a+b
    d = {'result': r,
         'test': 'hello'}
    return jsonify(d)


@app.route('/')
def hello():
    return render_template('index.html')

@app.route('/submit', methods=['GET', 'POST'])
def submit():
    form = RestoreForm(request.form, csrf_enabled=False)
    if request.method == 'POST':
        # keyword = form.keyword.data
        keyword = request.form['restoreKeyword']
        date_operator = request.form['restoreDateOperator']
        date = request.form['restoreDate']
        modifying_user = request.form['restoreModifyingUser']
        p = {'keyword': keyword,
             'date_operator': date_operator,
             'date': date,
             'modifying_user': modifying_user
             }
        print p
        service = authorize()
        search = SearchForFiles(service)
        #search = FindFiles(service)
        params = {'keyword': keyword}
        results = search.search_for_files(params)
        resp = json.dumps(results)
        return Response(resp)
        #return Response(resp)

    return render_template('test.html', form=form)

# @app.route('/getpythondata')
# def get_python_data():
#     return json.dumps()

@app.route('/hello')
def test(name=None):
    return render_template('restoreApp.html', name=name)

@app.route('/authorization')
def auth_user():
    authorize()
    return 'Authorized'

# @app.route('/revoke')
# def revoke_token():
#     revoke_access()

@app.route('/restore')
def restore_files(a):
    service = authorize()
    search = FindFiles(service)
    param = {'keyword': '.jpg'}
    file_ids = search.find_files(param)
    return "Files Found"

#@app.route('/rename')

if __name__ == "__main__":
    app.run(debug=True)