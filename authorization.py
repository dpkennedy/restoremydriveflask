# -*- coding: utf-8 -*-
"""
Description:

Author: David Kennedy
Copyright:
Credits
License
Version
Maintainer: David Kennedy
Email: david.kennedy48@gmail.com
Status: Development
"""

from __future__ import print_function
import httplib2
import os

import oauth2client
from apiclient import discovery
from oauth2client import client
from oauth2client import tools

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_FILE = 'DPK_Client.json'
APPLICATION_NAME = 'Restore My Drive'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'DPK_Client.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatability with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def authorize():
    """Gathers credentials, authorizes session, and builds a service
    for the application.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v2', http=http)

    return service


def revoke_access():
    credentials = get_credentials()
    credentials.revoke(httplib2.Http())

# def main():
#     authorize()
#
# if __name__ == '__main__':
#      main()
