# -*- coding: utf-8 -*-
import sys
import os
from time import sleep


class SearchForFiles(object):

    def __init__(self, service):
        self.service = service

    def search_for_files(self, param_input):
        service = self.service
        s_keyword = param_input['keyword']
        # s_date = param_input['date']
        # o_date = param_input['date-operator']
        # s_owner = param_input['owner']
        # s_mimetype = param_input['mimetype']
        q_string = ''
        file_ids = []


        # Create separate param strings based on form input
        if s_keyword != '':
            keyword_string = 'title contains "%s"' % s_keyword
        else:
            keyword_string = 'not title contains ""'
        # if s_date != '':
        #     date_string = 'date %s "%s"' % (o_date, s_date)
        # else:
        #     date_string = 'date != ""'
        # if s_owner != '':
        #     owner_string = '"%s" in owners' % s_owner
        # else:
        #     owner_string = 'not "" in owners'
        # if s_mimetype != '':
        #     mimetype_string = 'mimeType = "%s"' % s_mimetype
        # else:
        #     mimetype_string = 'mimeType != ""'

        # q_string = '%s and %s and $s and %s' % (keyword_string, date_string, owner_string, mimetype_string)
        # q_string = '%s and %s and $s' % (keyword_string, date_string, mimetype_string)
        q_string = keyword_string
        print 'q_string: ', q_string

        try:
            param = {'maxResults': '10',
                     'q': q_string,
                     'fields': 'items(id,lastModifyingUserName,mimeType,modifiedDate,title)'}

            files = service.files().list(**param).execute()

            for item in files['items']:
                    print item['title'], '||', item['id']
                    file_ids.append({'title': item['title'],
                                     'id': item['id']})
        except:
            print 'Error'

        return file_ids
